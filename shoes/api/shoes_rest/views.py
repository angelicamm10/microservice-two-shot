from django.shortcuts import render
from django.http import JsonResponse
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href"]

class ShoesDetailEncoder(ModelEncoder):
    model= Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders= {
        "bin": BinVODetailEncoder(),
    }

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "id",
        "bin"     
    ]
    encoders= {
        "bin": BinVODetailEncoder(),
    }
    # def get_extra_data(self, o):
    #     return {"bin": o.bin.href}



@require_http_methods(["GET","POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes=Shoes.objects.all()
        return JsonResponse(
            {"shoes":shoes}, 
            encoder=ShoesListEncoder, 
            
        )
    else: 
        content = json.loads(request.body)
        
        
        try:
            bin_href= content["bin"]
            bin= BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
            
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
                )
        shoe = Shoes.objects.create(**content),
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET",])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    else:
        count,_ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count > 0})

        
