from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hats


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["fabric","style_name","color","picture_url", "location"]
    encoders = {
        "location" : LocationVODetailEncoder(),
    }


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = ["fabric", "style_name", "color", "picture_url", "location"]
    encoders = {
        "location" : LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        attendee = Hats.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
