import React, {useEffect, useState,} from 'react';


function ShoesForm(props){

    const [bins, setBins]= useState([]);
    const [modelName, setModelName]=useState('');
    const [manufacturer, setManufacturer]=useState('');
    const [color, setColor]=useState('');
    const [pictureUrl, setPictureUrl]=useState('');
    const [bin, setBin]=useState('');
  


    const fetchData = async () =>{
        const url = 'http://localhost:8100/api/bins/';
  
        const response = await fetch(url);
  
        if (response.ok) {
        const data = await response.json();
        console.log(data);
        setBins(data.bins);
        }
    }

    useEffect(() => {fetchData();}, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data={};

    data.model_name =modelName;
    data.manufacturer = manufacturer;
    data.color= color;
    data.picture_url= pictureUrl;
    data.bin = bin;
    console.log(data);


    const shoesUrl = 'http://localhost:8080/api/shoes/';
        
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
        const newShoe = await response.json();
        console.log(newShoe);

    setModelName('');
    setManufacturer('');
    setColor('');
    setPictureUrl('');
    setBin('');
    } 
    }


  
    const handleModelNameChange= (event)=> {
        const value = event.target.value;
        setModelName(value);
    }
 
    const handleManufacturerChange= (event)=> {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange= (event)=> {
        const value = event.target.value;
        setColor(value);
    }


    const handlePictureChange= (event)=> {
        const value = event.target.value;
        setPictureUrl(value);
    }



    const handleBinChange= (event)=> {
        const value = event.target.value;
        setBin(value);
    }




    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add A Shoe</h1>
            <form  onSubmit={handleSubmit}id="create-shoe-form">
            <div className="form-floating mb-3">
                <input onChange={handleModelNameChange}placeholder="model-name" required type="text" id="model-name" name="model_name" value={modelName} className="form-control"/>
                <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="manufacturer" required type="text" id="manufacturer" name="manufacturer" value={manufacturer} className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="color" required type="text" name="color" value={color} id="color" className="form-control"/>
                <label htmlFor="ends">Color</label>
            </div>
            <div className="mb-3">
                <input onChange={handlePictureChange} placeholder="picture_url" type="url" name="picture_url" value={pictureUrl} id="picture_url" className="form-control"/>
                <label htmlFor="picture">Photo</label>
            </div>
            <div className="mb-3">
                <select onChange={handleBinChange} required id="bins" name="bin" value={bin} className="form-select">
                <option value='' >Choose a bin</option>
                {bins.map(bin => {
                    return (
                        <option key= {bin.id} value={bin.href}>
                            {bin.closet_name}
                        </option>
                    )
                })}
                </select>
            </div>
            <button className="btn btn-primary me-md-4">Create</button>
            </form>
            </div>
            </div>
        </div>
        
    )

}

export default ShoesForm;
