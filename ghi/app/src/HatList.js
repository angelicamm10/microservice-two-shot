import { useRef,useState } from 'react'
function HatList(props) {
  const delete_id = useRef(null)
  const [deleteHat, setHatResult] = useState(null)
  const url = "http://localhost:8090/api/hats"
  const formatResponse = (res) => {
    return JSON.stringify(res, null, )
  }
  async function deleteDataById() {
    const id = delete_id.current.value;

    if (id){
      try {
        const res = await fetch(`${url}/${id}`, { method: "delete" });

        const data = await res.json();

        const result = {
          status: res.status + "-" + res.statusText,
          headers: { "Content-Type": res.headers.get("Content-Type") },
          data: data,
        };

        setHatResult(formatResponse(result));}
        catch (err) {
          setHatResult(err.message)}}}
  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style</th>
            <th>Picture</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hats => {
            return (
              <tr key={hats.href}>
                <td>{ hats.style_name }</td>
                <td>
                  <img src={hats.picture_url}/>
                  </td>
                <td>{hats.fabric}</td>
                <td>{hats.color}</td>
                <td>{hats.location.import_href }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <input type="text" ref={delete_id} className="form-control" placeholder="Id" />
      <div className="input-group-append">
          <button className="btn btn-sm btn-danger" onClick={deleteDataById}>Delete by Id</button>
      </div>
      </div>
    );
  }

  export default HatList;
