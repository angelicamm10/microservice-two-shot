import { BrowserRouter, Routes, Route, Outlet} from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatList from './HatList';
import HatForm from './HatForm';


function App(props) {

  return (

    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path= "shoes">
            <Route path= "list" element= {<ShoesList shoes={props.shoes} />} />
            <Route path="new" element = {<ShoesForm />} />
          </Route>
          <Route path= "hats">
            <Route path="list" element = {<HatList hats={props.hats} />} />
            <Route path="new" element = {<HatForm />} />
          </Route>
        </Routes>
        <Outlet />
      </div>
    </BrowserRouter>
  );
}

export default App;
