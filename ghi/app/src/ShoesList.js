
import React, {useEffect, useState,} from 'react';
import App from './App';




function ShoesList(props){

    const [shoes, setShoes] = useState([]);



    async function loadShoes() {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setShoes(data.shoes)

        }
        }
        useEffect(() => {loadShoes();}, []);
    
    
    const handleClickDelete = async (shoeId)=>{
        const deleteUrl = `http://localhost:8080/api/shoes/${shoeId}`;

        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(deleteUrl, fetchConfig);

        if(response.ok){
            const data = await response.json();
            
        }
    }
    
    
    
    return(
        <table className = "table table-striped">
            <thead>
                <tr>

                <th>Model Name</th>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return(
                        
                        <tr key={shoe.href}> 
                            <td>{shoe.model_name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin.closet_name}</td>
                            <button onClick={()=>handleClickDelete(shoe.id)} className="btn btn-primary me-md-4">Delete</button>
                            

                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}
export default ShoesList;
