# Generated by Django 4.0.3 on 2023-06-01 19:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wardrobe_api', '0002_bin_alter_location_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bin',
            name='bin_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='bin',
            name='bin_size',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
