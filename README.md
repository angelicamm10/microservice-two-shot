# Wardrobify

Team:

* Person 1 - Which microservice?
* Angelica Melo - shoes

## Design

## Shoes microservice

The shoes model will have the fields to keep track of:
-manufacturer
-model name
-color
-URL for a picture
-the bin in the wardrobe where it exists.
The bin will be information that the shoes microservice will obtain from the wardrobe API.


Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
-
I will first create a model for the Hats with the required properties and using the locationVO as a foreign key. Then I will create RESTul APIs that will use the ModelEncoder given in the json.py. These RESTful API-views will then be given URLs to match. Then I will work on the poller for Location model by creating a Location VO model with the required properties and writing a function in the poller.py file to populate the LocationVO with data.
